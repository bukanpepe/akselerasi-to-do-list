import moment from 'moment';
import { useEffect, useState } from 'react';
import { Trash2 } from 'react-feather';
import ReactPaginate from 'react-paginate';
import { useDispatch, useSelector } from 'react-redux';
import { Container, FormGroup, Input, Label, Row, Col, Form, Button, Card, CardHeader, CardBody, Offcanvas, OffcanvasHeader, OffcanvasBody } from 'reactstrap';
import './App.css';
import { deleteTask, getData, newTask, updateTaskData, updateTaskItem } from './redux/reducers/data';

function App() {
  const dispatch = useDispatch();
  const store = useSelector(state => state.dataReducer);

  const [noInput, setNoInput] = useState(0),
    [taskTitle, setTaskTitle] = useState(''),
    [taskDate, setTaskDate] = useState(''),
    [newTasks, setNewTasks] = useState([]);

  const [displayList, setDisplayList] = useState([]),
    [totalCount, setTotalCount] = useState(0);

  const [itemsPerPage, setItemsPerPage] = useState(5),
    [currentPage, setCurrentPage] = useState(1);

  const [selectedItem, setSelectedItem] = useState({}),
    [editedItem, setEditedItem] = useState({}),
    [openCanvas, setOpenCanvas] = useState(false)

  useEffect(() => {
    dispatch(
      getData({
        page: currentPage,
        rows: itemsPerPage
      })
    )
  }, [currentPage])

  useEffect(() => {
    setDisplayList(store.list);
    setTotalCount(store.total);
  }, [store])

  useEffect(() => {
    const temp = {
      text: '',
      checked: false
    }
    if (noInput > newTasks.length) {
      setNewTasks(newTasks => [...newTasks, temp])
    } else {
      if (noInput) {
        let newData = newTasks.slice(0);
        newData.splice(newData.length - 1);

        setNewTasks(newData)
      } else {
        setNewTasks([])
      }
    }
  }, [noInput])

  const updateTask = (i, val) => {
    let newData = newTasks.slice(0);
    newData[i].text = val;
    setNewTasks(newData);
  }

  const handleAddTask = () => {
    dispatch(
      newTask({
        title: taskTitle,
        date: taskDate || new Date(),
        tasks: newTasks
      })
    ).then(res => {
      if (res === 'ok') {
        dispatch(
          getData({
            page: currentPage,
            rows: itemsPerPage
          })
        );
      }
    });

    setNewTasks([]);
    setTaskTitle('');
    setTaskDate('');
    setNoInput(0);
  }

  const updateIndividual = (parentIndex, selfIndex, key, value) => {
    dispatch(
      updateTaskItem({
        parentIndex,
        selfIndex,
        key,
        value,
        page: currentPage,
        rows: itemsPerPage
      })
    ).then(res => {
      if (res === 'ok') {
        dispatch(
          getData({
            page: currentPage,
            rows: itemsPerPage
          })
        );
      }
    });
  }

  const handleDelete = (index) => {
    dispatch(
      deleteTask({
        index,
        page: currentPage,
        rows: itemsPerPage
      })
    ).then(res => {
      if (res === 'ok') {
        dispatch(
          getData({
            page: currentPage,
            rows: itemsPerPage
          })
        );
      }
    });
  }

  const handlePageClick = (event) => {
    setCurrentPage(event.selected + 1);
  };

  const toggleCanvas = () => setOpenCanvas(!openCanvas);

  const toggleEdit = (item, index) => {
    const data = {
      title: item.title,
      index
    }
    setSelectedItem(data);
    setEditedItem(item);
    toggleCanvas();
  }

  const handleConfirmEdit = () => {
    dispatch(
      updateTaskData(
        {
          index: selectedItem.index,
          page: currentPage,
          rows: itemsPerPage,
          data: editedItem
        }
      )
    ).then(res => {
      if (res === 'ok') {
        dispatch(
          getData({
            page: currentPage,
            rows: itemsPerPage
          })
        );
        setTimeout(() => {
          toggleCanvas();
          setSelectedItem([]);
          setEditedItem({});
        }, 500);
      }
    });
  }

  return (
    <Container className='py-5 px-4'>
      <h1 className='text-center'>To-Do List</h1>
      <Form className='w-75 mt-5 mx-auto'>
        <FormGroup row>
          <Col sm={6}>
            <Label for="taskTitle">
              Title
            </Label>
            <Input
              type='text'
              id="taskTitle"
              value={taskTitle}
              onChange={e => setTaskTitle(e.target.value)}
            />
          </Col>
          <Col sm={4}>
            <Label for="taskDate" >
              Date
            </Label>
            <Input
              type='date'
              id="taskDate"
              value={taskDate}
              onChange={e => setTaskDate(e.target.value)}
            />
          </Col>
          <Col sm={2}>
            <Label for="noInput" >
              # of Input
            </Label>
            <Input
              type='number'
              id="noInput"
              value={noInput}
              onChange={e => setNoInput(e.target.value)}
            />
          </Col>
        </FormGroup>
        {newTasks.length ? <Row className='mt-4'>
          <Col sm={12}>
            {newTasks.map((item, index) => {
              return <Input
                type='text'
                className='mb-2'
                placeholder='New task'
                key={index}
                onChange={e => updateTask(index, e.target.value)}
              />
            })}
          </Col>
          <Col sm={12} className="mt-3 d-flex justify-content-center">
            <Button outline color='primary' onClick={handleAddTask}>
              Add Task
            </Button>
          </Col>

        </Row> : null}

        {displayList.length ? <hr className='my-5' /> : null}

        {displayList.length ? displayList.map((item, index) => {
          return <Row key={index} className="mb-4">
            <Col sm={12}>
              <Card>
                <CardHeader>
                  <div className='d-flex align-items-baseline justify-content-between mt-2'>
                    <div className='d-flex align-items-baseline'>
                      <h4>{item.title}</h4>
                      <h6
                        className={`fw-bold ms-3 ${moment(item.date).format('ddd, DD/MM/yyyy') === moment().format('ddd, DD/MM/yyyy') ? 'text-info' : 'text-muted'}`}
                      >
                        {moment(item.date).format('ddd, DD/MM/yyyy')}
                      </h6>
                    </div>
                    <div className='d-flex'>
                      <Button size='sm' outline color="info" className='me-2 w-100' onClick={() => toggleEdit(item, index)}>
                        Edit
                      </Button>
                      <Button size='sm' outline color="danger" className='w-100' onClick={() => handleDelete(index)}>
                        Delete
                      </Button>
                    </div>
                  </div>
                </CardHeader>
                <CardBody>
                  {item.tasks.length ? item.tasks.map((det, j) => {
                    return <Row key={j} className="task-item mb-2 py-1">
                      <Col sm={1} className="text-center border-end">
                        <Input
                          type="checkbox"
                          checked={det.checked}
                          onChange={e => updateIndividual(index, j, 'checked', e.target.checked)}
                        />
                      </Col>
                      <Col sm={10} className={det.checked ? 'text-decoration-line-through' : ''}>
                        {det.text}
                      </Col>
                      <Col sm={1} className="d-flex align-items-center justify-content-center border-start cursor-pointer" onClick={() => updateIndividual(index, j, 'delete', null)}>
                        <Trash2 size={20} />
                      </Col>
                    </Row>
                  }) : null}
                </CardBody>
              </Card>
            </Col>
          </Row>
        }) : null}
        <ReactPaginate
          className='react-paginate d-flex justify-content-center'
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          pageCount={Math.ceil(totalCount / itemsPerPage) || 0}
          previousLabel=""
          renderOnZeroPageCount={null}
          previousClassName="arrow left"
          nextClassName='arrow right'
        />
      </Form>
      {openCanvas && <Offcanvas
        toggle={toggleCanvas}
        isOpen={openCanvas}
        direction="end"
      >
        <OffcanvasHeader toggle={toggleCanvas}>
          Edit <strong>{selectedItem.title}</strong>
        </OffcanvasHeader>
        <OffcanvasBody>
          <Form>
            <FormGroup>
              <Label for="editInput">
                Title
              </Label>
              <Input
                type='text'
                id="editTitle"
                value={editedItem.title}
                onChange={e => setEditedItem({ ...editedItem, title: e.target.value })}
              />
            </FormGroup>
            <FormGroup>
              <Label for="editDate" >
                Date
              </Label>
              <Input
                type='date'
                id="editDate"
                value={editedItem.date}
                onChange={e => setEditedItem({ ...editedItem, date: e.target.value })}
              />
            </FormGroup>
            {editedItem.tasks.length ? editedItem.tasks.map((item, index) => {
              return <FormGroup key={index}>
                <Label for={`task${index}`} >
                  Task #{index + 1}
                </Label>
                <Input
                  type='text'
                  id={`task${index}`}
                  value={editedItem.tasks[index].text}
                  onChange={e => setEditedItem({ ...editedItem, tasks: [...editedItem.tasks.slice(0, index), { text : e.target.value, checked: false }, ...editedItem.tasks.slice(index + 1)] })}
                />
              </FormGroup>
            }) : null}
            <Button block color='info' onClick={() => handleConfirmEdit()}>
              Confirm Edit
            </Button>
          </Form>
        </OffcanvasBody>
      </Offcanvas>}
    </Container>
  );
}

export default App;
