import mock from '../mock'

const paginateArray = (array, perPage, page) => array.slice((page - 1) * perPage, page * perPage)

mock.onPost('/api/getData').reply(config => {
    const body = JSON.parse(config.data);

    const stored = window.localStorage.getItem('todo-list');
    let storedData;
    let filteredData;

    if (stored) {
        storedData = JSON.parse(stored);
        storedData.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
        filteredData = paginateArray(storedData, body.rows, body.page);
    }

    return [200, {
        data: filteredData,
        count: storedData.length
    }]
});

mock.onPost('/api/newTask').reply(config => {
    const body = JSON.parse(config.data);

    const stored = window.localStorage.getItem('todo-list');
    let storedData = [];

    if (stored) {
        storedData = JSON.parse(stored);
    }
    storedData.push(body)

    const dataToStore = JSON.stringify(storedData);
    window.localStorage.setItem('todo-list', dataToStore);

    return [200]
});

mock.onPost('/api/deleteTask').reply(config => {
    const body = JSON.parse(config.data);

    const stored = window.localStorage.getItem('todo-list');
    let storedData = [];
    let newData;

    if (stored) {
        storedData = JSON.parse(stored);
        storedData.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
        newData = storedData.slice(0);
    }
    newData.splice(body.index + ((body.page - 1) * body.rows), 1);

    const dataToStore = JSON.stringify(newData);
    window.localStorage.setItem('todo-list', dataToStore);

    return [200]
});

mock.onPost('/api/updateTaskData').reply(config => {
    const body = JSON.parse(config.data);

    const stored = window.localStorage.getItem('todo-list');
    let storedData = [];
    let newData;

    if (stored) {
        storedData = JSON.parse(stored);
        storedData.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
        newData = storedData.slice(0);
    }

    newData[body.index + ((body.page - 1) * body.rows)] = body.data;
    
    const dataToStore = JSON.stringify(newData);
    window.localStorage.setItem('todo-list', dataToStore);

    return [200]
});

mock.onPost('/api/updateTaskItem').reply(config => {
    const body = JSON.parse(config.data);

    const stored = window.localStorage.getItem('todo-list');
    let storedData = [];
    let newData;

    if (stored) {
        storedData = JSON.parse(stored);
        storedData.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
        newData = storedData.slice(0);
    }

    if (body.key === 'delete') {
        newData[body.parentIndex + ((body.page - 1) * body.rows)].tasks.splice(body.selfIndex, 1);
    } else {
        newData[body.parentIndex + ((body.page - 1) * body.rows)].tasks[body.selfIndex][body.key] = body.value;
    }

    const dataToStore = JSON.stringify(newData);
    window.localStorage.setItem('todo-list', dataToStore);

    return [200]
});