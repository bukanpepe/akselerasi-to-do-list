import axios from "axios"

const initialState = {
    list: [],
    total: 0
}

export const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_DATA':
            return { ...state, list: action.data.data, total: action.data.count }
        default:
            return state
    }
}

//INDIVIDUAL ACTION WITH API CALL
export const getData = params => {
    return dispatch => {
        return axios.post('/api/getData', params)
            .then(response => {
                dispatch({
                    type: 'SET_DATA',
                    data: response.data
                })
            });
    }
}

export const newTask = params => {
    return dispatch => {
        return axios.post('/api/newTask', params)
            .then(response => {
                if (response.status === 200) {
                    return 'ok'
                }
            });
    }
}

export const deleteTask = params => {
    return dispatch => {
        return axios.post('/api/deleteTask', params)
            .then(response => {
                if (response.status === 200) {
                    return 'ok'
                }
            });
    }
}

export const updateTaskData = params => {
    return dispatch => {
        return axios.post('/api/updateTaskData', params)
            .then(response => {
                if (response.status === 200) {
                    return 'ok'
                }
            });
    }
}

export const updateTaskItem = params => {
    return dispatch => {
        return axios.post('/api/updateTaskItem', params)
            .then(response => {
                if (response.status === 200) {
                    return 'ok'
                }
            });
    }
}