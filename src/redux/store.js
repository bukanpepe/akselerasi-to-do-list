import { Provider } from 'react-redux'
import thunk from 'redux-thunk';

import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { dataReducer } from './reducers/data';

const middleware = [thunk];

const rootReducer = combineReducers({ dataReducer });

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// ** Create store
const store = createStore(rootReducer, {}, composeEnhancers(applyMiddleware(...middleware)))

export const StoreProvider = ({ children }) => {
    return <Provider store={store} >
        {children}
    </Provider>
}
